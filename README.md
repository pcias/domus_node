#domus_node
Yet another simple implementation of a web interface for heyu 
(X10 home automation).

Advantages over other similiar things:

-small footprint (node.js, no apache, no php)

-runs really fast

-default mobile interface

->runs very well on embedded/prototype platforms:

->tested on beaglebone (armv7) (ubuntu 10.10), node 0.4

->kt-sbc-sam9 (www.kristech.eu, armv5tej) (angstrom linux distibution), node 0.4

Web presentation is mobile webkit optimized, works nice in iphone, ipad, 
BlackeBerry OS 6 and up , also not bad on desktop.

##Prerequisities
heyu installed (www.heyu.org)

Tested on Linux (angstrom, ubuntu) (arm5, arm7 platforms, however is 
the code pure javascript architecture agnostic)

Depends on paperboy, http-auth2 node modules, they are statically linked here at least for now.


## Installation
Method 1. 

npm install domus_node

Method 2. 

Clone this repository with git.


##Configuration


Configuration self-descriptive in the config.js file.
Please note switching between production config and development config in the 
following of the forementioned file: 

...
//config binding -> change here when moving between development and production
exports.config=development_config;
...

So before starting you have to make sure you have config.js edited to reflect your 
system paths, etc. in your production_config object and then change the line above 
to something like: 
exports.config=production_config;

House/Units setup is read from heyu's x10.conf file.
Aliased are read from there as standrd, however note that groupings are after 
hash comments e.g. x10.conf file included:

...

alias Living_Room B7 AM12 # GroundFloor                                                    
alias Bedroom B8 AM12 # GroundFloor                                            
alias Stairs C2 AM12 # Stairs        
...

alias <alias_name> <address> # <group>

No whitespace allowed in any of the <items> above.

Units in each group should belong to same house code for the big 'group off'
buttons to work properly

##Usage

As simple as:

node index.js

However recommended is prior installation of 'forever' module  (npm install -g forever); 
included version of http-auth2 tends to crash from time to time, so forever 
takes care of restarting (ala 'forever start index.js' in your startup scripts).


## License

The GPL version 3, read it at [http://www.gnu.org/licenses/gpl.txt](http://www.gnu.org/licenses/gpl.txt)