//config sets

var production_config = {  
    'x10confpath' : '/etc/heyu/x10.conf',
    'heyuExec' : '/usr/local/bin/heyu',
    'debug' : false,
    'authenticatedPort' : 8888,
    'nonauthenticatedPort' : 8889,
    'wireless' : false     // true for CM17A (firecracker)
};

var development_config = {
    'x10confpath' : './testenv/x10.conf',
    'heyuExec' : '/usr/local/bin/heyu',
    'debug' : true,
    'authenticatedPort' : process.env.PORT,
    'nonauthenticatedPort' : process.env.PORT,
    'wireless' : false    // true for CM17A (firecracker)
};

//config binding -> change here when moving between development and production
exports.config=development_config;


//this is for the future. not found yet a decent http-auth module that would use md5 hashes 
//username : MD5 hashed passwords
//generate from linux shell level with $ echo your_password | md5sum
/*exports.users  = {
    'klimek':'7256dc19f86e242e59b574645794bd70',
    'zaba':'111647f89626efc6f2cc79be0e3e5e5c',
    'slon': 'fbfec43da4bf66a0a1ea4c7372e076e7'
}
*/
//for now - ugly and insecure
exports.users  = {
    'user' : 'pass'
}
