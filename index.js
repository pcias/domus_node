var server=require('./server.js');
var router=require('./router.js');
var requestHandlers=require('./requestHandlers');

var handle={};
handle['/']=requestHandlers.start;
handle['/start']=requestHandlers.start;
handle['/heyu']=requestHandlers.heyu;
handle['/heyuAjax']=requestHandlers.heyuAjax;
handle['/heyuGroupActionAjax']=requestHandlers.heyuGroupActionAjax;

console.log('index starting...');
server.start(router.route, handle);
