
//Logic
    var device = '';
    var delay = 500;

    //utility function to parse and read a parameter from current request url
    function getUrlParameter( name ) {
      name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
      var regexS = "[\\?&]"+name+"=([^&#]*)";
      var regex = new RegExp( regexS );
      var results = regex.exec( window.location.href );
      if( results == null )
        return "";
      else
        return results[1];
    }


    var state = 'none';
    //display panel
    function showhide(layer_ref) {

	    if (state == 'block') {
	        state = 'none';
	    }
	    else {
	        state = 'block';
	    }
	    if (document.getElementById && !document.all) {
	        hza = document.getElementById(layer_ref);
	        hza.style.display = state;
	    }
	}


	window.onload = function() {
	    device = getUrlParameter('device')

        var sliderValue = getUrlParameter('comparam');
        if (sliderValue === '') sliderValue = 1 ;

        document.getElementById('slider').value = sliderValue ;
        setTimeout(function() {
	        window.scrollTo(0, 1);
	    }, 100);
	};


    //doing hard way
    function showPanel() {
        document.getElementById('optionpanel').style.display='block';
    }






    //I should change this into an object, this is ugly now
    var sliderTimeout;

    function sliderMechanic(device,command,comparam) {
        clearTimeout(sliderTimeout);
//        alert('nodeHeyuCall(\''+device+'\',\''+command+'\',\''+comparam+'\')');
         sliderTimeout=setTimeout('nodeHeyuCall(\''+device+'\',\''+command+'\',\''+comparam+'\')',500);
    }


    //helper fuction for redirecting calls to heyu request url of nodejs
    //makes inline javascript look nicer and simplier
    
    //non-ajax version
    function nodeHeyuCallNoAjax(device,command,comparam) {
                window.location.href='heyu?device='+device+'&command='+command+'&comparam='+comparam;
    }
    //ajax version
    function nodeHeyuCallAjax(device,command,comparam) {
                $.post('heyuAjax', { 'device': device, 'command': command, 'comparam': comparam });
    }
    
    function nodeHeyuCallGroupActionAjax(group,command) {
                $.post('heyuGroupActionAjax', { 'group': group, 'command': command });
    }

    nodeHeyuCall=nodeHeyuCallAjax;
    
//graphics
    $(document).ready(function () {
        $(".button").hover(function() { $(this).addClass("black");},
                          function() { $(this).removeClass("black");})
    });
