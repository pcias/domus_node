var exec=require("child_process").exec;
var querystring=require("querystring");
var url=require("url");
var x10=require("./x10");
var config=require('./config.js');

function start(request,response) {
  console.log("Request handler 'start' was called.");

  x10.draw_devices(response,'none');
}


function heyu(request, response) {
    var device = querystring.parse(url.parse(request.url).query)["device"];
    var command = querystring.parse(url.parse(request.url).query)["command"];
    var comparam = querystring.parse(url.parse(request.url).query)["comparam"];

    console.log("Request handler 'heyu' was called.");
    x10.execHeyu(device,command, comparam);
    x10.draw_devices(response,'block');
}


// handle a POST request and parse out the values
function handlePost( request, callback ) {
    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;
            if (body.length > 1024) {
                // FLOOD ATTACK OR FAULTY CLIENT, NUKE REQUEST
                request.connection.destroy();
            }
        });

        request.on('end', function () {
            var post = querystring.parse(body);
            callback( post );
        });
    }
}

function heyuAjax(request, response) {
    handlePost( request, function( post ) {
        var device = post["device"];
        var command = post["command"];
        var comparam = post["comparam"];

        console.log("Request handler 'heyuAjax' was called.");
        x10.execHeyu(device,command,comparam);
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('heyu done\n');
    });
}

function heyuGroupActionAjax(request,response) {
    handlePost( request, function( post ) {
        var group = post["group"];
        var command = post["command"];
    
        x10.groupX10Action(group, command);

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('groupActionAjax done\n');
    } );
}

exports.start = start;
exports.heyu = heyu;
exports.heyuAjax = heyuAjax;
exports.heyuGroupActionAjax = heyuGroupActionAjax;
