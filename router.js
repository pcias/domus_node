

function route(handle, pathname, request, response) {
    if (typeof handle[pathname] === 'function') {
        console.log("Routing :" + pathname);
        handle[pathname](request, response);
    }
    else {
        //fall back to paperboy
        var paperboy = require('paperboy');
        
        var ip = request.connection.remoteAddress;
        var path = require('path');
        var webroot = path.join(__dirname, '');

        paperboy
        .deliver(webroot, request, response)
        .addHeader('X-PaperRoute', 'Node')
        .before(function() {
            //console.log('Request received for ' + request.url);
        })
        .after(function(statusCode) {
            //console.log(statusCode + ' - ' + request.url + ' ' + ip);
        })
        .error(function(statusCode, msg) {
            //console.log([statusCode, msg, request.url, ip].join(' '));
            response.writeHead(statusCode, {
                'Content-Type': 'text/plain'
            });
            response.end('Error [' + statusCode + ']');
        })
        .otherwise(function(err) {
            //console.log([404, err, request.url, ip].join(' '));
            response.writeHead(404, {
                'Content-Type': 'text/plain'
            });
            response.end('Error 404: File not found');
        });
    }
}

exports.route = route;