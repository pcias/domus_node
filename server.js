var config=require('./config.js');

var httpAuth = require('http-auth2');
var http = require('http');
var url = require('url');



function start(route,handle) {

  function onRequest(request,response) {
      var pathname = url.parse(request.url).pathname;
      console.log("request.url: "+request.url);
      route(handle,pathname,request,response);
//      response.end();
  }
  
  function authenticate(user) {
      password = config.users[user];
      if(password!=null)
        return password;
        else
        return false;
  }
  
//to be changed to http_digest_auth(request, response, username, password, callback)
//  httpdigest.createServer("aaa","qaz",onRequest).listen(config.config["port"]);
//changed to http-auth2
//first sever created for authenticated access from the Internet. This port can be added to your router rules
httpAuth.createServer(authenticate,onRequest).listen(config.config["authenticatedPort"]);

//second server created for non-authenticated access from LAN. This port should not be routed to the Internet
if (config.config["nonauthenticatedPort"]!=config.config["authenticatedPort"])
    http.createServer(onRequest).listen(config.config["nonauthenticatedPort"]);

}

exports.start=start;