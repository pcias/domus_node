var config=require('./config.js');


var x10config = {};


function execHeyu(device, command, comparam) {
        // validate command
        switch ( command ) {
        case 'alloff':
        case 'obdim':
        case 'on':
        case 'off':
            break; 
        default:
            console.log( 'Error: invalid command ' + command );
            return;
        }

        var exec = require('child_process').execFile;
        if ( config.config.wireless ) {
            switch ( command ) {
            case 'alloff':
                command = 'falloff';
                break;
            case 'obdim':
                // no direct command for this with firecracker - must brighten then dim
		command = 'ffbright ' + device + ' 22\; ffdim';
                break;
            case 'on':
                command = 'fon';
                break;
            case 'off':
                command = 'foff';
                break;
            }
        }

        console.log('Exec> '+config.config.heyuExec+' '+command+' '+device+' '+comparam);
        if (!config.config.debug) {
            exec(config.config.heyuExec, [ command+' '+device+' '+comparam ], function(error, stdout, stderr) {
                        console.log('stdout: ' + stdout);
                        console.log('stderr: ' + stderr);
                        if (error !== null) {
                            console.log('exec error: ' + error);
                        }
            });
        }
}


//reads x10.conf file, parsing for aliases in order to populate the x10config object
//assumed is alias line in format "alias <alias_name> <module_address> <module_type> # <group>
function readX10config() {
    var fs=require('fs');
    var x10confObject={};

    var x10confData = fs.readFileSync(config.config["x10confpath"],encoding='ASCII');
    var pattern = new RegExp('\nalias.*','ig');
    
    var match=[];
    while((match=pattern.exec(x10confData))!=null) {
        var line=match[0].split(/[ \t]+/);
        if ( line[3] == '#' )
            x10confObject[line[1]]={'alias':line[1],'address':line[2],'group':line[4].replace('_', ' '), 'groupHouse':line[2].substring(0,1)};
        else
            x10confObject[line[1]]={'alias':line[1],'address':line[2],'group':line[5].replace('_', ' '), 'groupHouse':line[2].substring(0,1)};
    }
   
    return x10confObject;
}

//executed action on all devices in a group
//this is not used anymore due to challenges relatede to locking of multiple consecutive heyu commands
//but leaving this out as an example of function generation within a loop
function groupX10Action(group, action) {
    var timeoutObj = {};
    
    console.log('groupX10Action '+group+' '+action);
    for(device in x10config) {
        if(x10config[device].group == group) {
            timeoutObj[device]=setTimeout((function(device,action) { 
                                                                        return function () { execHeyu(device,action,'');}; 
                                                                   })(device,action), 1250);
        }
    }
}

function writePgHead(response) {
  response.write('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">');
  response.write('<head>');
  response.write('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes" />');
  response.write('<link rel="stylesheet" href="UiUIKit/stylesheets/iphone.css" />');
//experimenal jquery mobile css
//response.write('<link rel="stylesheet" href="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.css" />');
//experimental end 
  response.write('<link rel="stylesheet" href="UiUIKit/stylesheets/pcsextras.css" />');
  response.write('<link rel="apple-touch-icon" href="UiUIKit/images/apple-touch-icon.png" />');
  response.write('<script src="./pagescripts/jquery-1.7.2.js"></script>');
  response.write('<script src="./pagescripts/pagescripts.js"></script>');
  response.write('</head>');
}

function draw_devices(response,panelDisplayStyle) {
    x10config = readX10config();
    
    if(config.config.debug==true) {
        console.log(x10config);
    }
    
    response.writeHead(200, {"Content-Type": "text/html"});
    writePgHead(response);
    response.write('<body>');
    
    response.write('<div id="header"><h1>domus Node</h1><div id="heyuactive" style="display: none"><a href="#" class="nav Action">heyu is active</a></div></div>');


    //sorting grops
    var x10sortable=[];
    for(var alias in x10config) {
        x10sortable.push(x10config[alias]);
    }
    x10sortable.sort(function(a,b) { 
                return (a.group < b.group? -1 : (a.group == b.group ? (a.alias < b.alias ? -1 : 1) : 1));
    });
    
    var prevgroup = '';
    for(var i in x10sortable) {
      if (x10sortable[i].group != prevgroup) {
      //draw here new group 
          if(prevgroup != '') {
             //if this is not the first group
             response.write('</ul>');
          }
          //response.write('<a href="#" class="white button" onclick="nodeHeyuCallGroupActionAjax(\''+x10sortable[i].group+'\',\'off\');return false;">'+x10sortable[i].group+'</a>');
          response.write('<a href="#" class="white button" onclick="nodeHeyuCall(\''+x10sortable[i].groupHouse+'\',\'alloff\',\'\');return false;">'+x10sortable[i].group+' ('+x10sortable[i].groupHouse+')</a>');
          response.write('<ul>');
      }
      response.write('<li>');
//      response.write('<a href=# onclick="showhide(\'optionpanel\');device=\''+x10sortable[i].alias+'\';">'+ x10sortable[i].alias+' '+x10sortable[i].address+'</a>');
      response.write('<a href="#" class="green button inline" onclick="device=\''+x10sortable[i].alias+'\';nodeHeyuCall(device,\'on\',\'\');return false;">ON</a> <a href="#" class="red button inline" onclick="device=\''+x10sortable[i].alias+'\';nodeHeyuCall(device,\'off\',\'\');return false;">OFF</a> <a href="#" class="white button inline" onclick="showhide(\'optionpanel\');device=\''+x10sortable[i].alias+'\';return false;">' + x10sortable[i].alias.replace( '_', ' ' ) + ' (' + x10sortable[i].address + ')</a>');

      response.write('</li>');
      prevgroup = x10sortable[i].group;
    }
    
    response.write('<div id="optionpanel" style="display: none">'
//                    +'<p>Control <script type="text/javascript">document.write(device);</script></p>'
                    +'<a href="#" class="black button" onclick="showhide(\'optionpanel\');">Hide</a>'
//                    +'<a href="#" class="red button" onclick="nodeHeyuCall(device,\'off\',\'\');">OFF</a>'
//                    +'<a href="#" class="green button" onclick="nodeHeyuCall(device,\'on\',\'\');">ON</a>'
                    +'<div class="iphone-slider"> <input type="range" name="slider" id="slider"  min="1" max="22" onchange="sliderMechanic(device,\'obdim\',this.value);"/></input><span>slide to dim</span></div>'
                    +'</div>'
                    );
    response.write('</body>');
    response.end();
}

exports.draw_devices=draw_devices;
exports.execHeyu=execHeyu;
exports.groupX10Action=groupX10Action;
